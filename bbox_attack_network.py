'''
This module performs Substitute network training with jacobian data augmentation.
'''
from cleverhans.attacks_tf import jacobian_graph, jacobian_augmentation

from keras.models import Sequential
from keras.layers import Dense
from sklearn.model_selection import train_test_split
from six.moves import xrange
import numpy as np
import tensorflow as tf

import dataprocessing as dp
import fingerbank_api as fb

AUG_BATCH_SIZE = 512
LMBDA = .1
RHO_MAX = 6
NB_CLASSES = 2

def train():
    '''
    Builds substitute network.
    Performs training with JDA.
    '''
    sess = tf.Session()
    device_class = input('Enter target device label: ')
    dp.process_data(device_class, sample_limit=60)
    print('Loading processed data.')
    dataset = np.load('processed.npz', allow_pickle=True)['arr_0']
    x_test = []
    y_test = []
    for i in range(int(len(dataset)/2)):
        x_test.append(dataset[i][0])
        y_test.append(dataset[i][1])
    x_sub = np.array(x_test)
    y_sub = np.array(y_test)
    print('Loading complete.\nConstructing Keras model.')
    model = Sequential()
    model.add(Dense(128, input_dim=253, activation=tf.nn.sigmoid))
    model.add(Dense(128, activation=tf.nn.sigmoid))
    model.add(Dense(128, activation=tf.nn.sigmoid))
    model.add(Dense(128, activation=tf.nn.sigmoid))
    model.add(Dense(2, activation=tf.nn.softmax))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.save_weights('initial_weights.m5')
    print('Construction complete.\nInitiating model training.')
    x_tensor = tf.placeholder(tf.float32, shape=(None, 253))
    training_hist = ''
    for rho in xrange(RHO_MAX):
        model.load_weights('initial_weights.m5')
        print('Substitute training epoch #', str(rho))
        hist = model.fit(x_sub, y_sub, epochs=10, batch_size=128)
        training_hist += 'Substitute epoch #' + str(rho) + '\n'
        training_hist += str(hist.history) + '\n'
        preds_sub = model(x_tensor)
        sess.run(tf.local_variables_initializer())
        sess.run(tf.global_variables_initializer())
        # Last iteration, don't augment data.
        if rho == RHO_MAX-1:
            break
        grads = jacobian_graph(preds_sub, x_tensor, NB_CLASSES)
        y_sub_int = np.array(list(map(np.argmax, y_sub)))

        # Perform Jacobian-based Data Augmentation
        lmbda_coef = 2 * int(int(rho / 3) != 0) - 1
        x_sub = jacobian_augmentation(sess, x_tensor, x_sub, y_sub_int, grads, lmbda_coef * LMBDA,
                                      AUG_BATCH_SIZE)
        x_unlabeled = x_sub[int(len(x_sub)/2):]

        # Label new data
        print('Labeling new fingerprint data...')
        x_unlabeled = np.array(list(map(lambda x: np.array(list(map(lambda y: 1 if y > 1 else
                                                                    (0 if y < 0 else y), x))),
                                        x_unlabeled)))
        fingerprints = list(map(dp.vector_to_fingerprint, x_unlabeled))
        labeled_fp = fb.auto_fb_req(fingerprints)
        new_fp_list, new_label_list = map(list, zip(*labeled_fp))
        new_fp_list = np.array(new_fp_list)
        new_label_list = np.array(list(map(lambda x: dp.label_to_vector(x, device_class),
                                           new_label_list)))
        y_sub = np.append(y_sub, new_label_list, axis=0)
    print('Model training complete.')
    model.save('model.h5')
    with open('training_hist', 'w') as file:
        file.write(training_hist)

train()
