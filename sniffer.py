'''
Module for sniffing packet headers and convert them into fingerprints
'''
import socket
import struct
import sys
import time
import parsers as p

API_KEY = "key=" #Insert your fingerbank API key

def sniff():
    '''
    Packet sniffing, stores the fingerprints gathered every 10th second to loops forever.
    '''

    start_time = time.time()
    try:
        connection = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(3))
    except socket.error as err_msg:
        print("Socket creation error: {}".format(err_msg))
        sys.exit()
    fingerprint_querries = []

    while True:
        flow_id = dnt_frgmnt = id_num = header_length = 0
        fb_query_body = {}
        protocol = 0
        # Start raw connection
        raw_data, _addr = connection.recvfrom(65536)

        # Unpack MAC address
        _dst_mac, src_mac, eth_protocol, data = ethernet_frame(raw_data)

        # Put source mac into query body
        fb_query_body['mac'] = src_mac.lower()

        ######## IPv6 ########
        # Ethernet protocol 56710 for IPv6
        if eth_protocol == 56710:
            (version, flow_id, _payload_length, protocol, ttl, _src, dst, data) = ipv6_packet(data)

        ######## IPv4 ########
        # Ethernet protocol 8 for IPv4
        if eth_protocol == 8:
            (version, header_length, ecn, id_num, mbz, dnt_frgmnt, ttl, protocol, _src, dst,
             data) = ipv4_packet(data)

        ######## IP protocol 6 for TCP ########
        if protocol == 6:
            (src_port, dst_port, sequence, acknowledgement, flag_urg, flag_ack, flag_psh, _flag_rst,
             flag_syn, _flag_fin, wsize, _checksum, urg_ptr, options, data) = tcp_segment(data)

            # Identify TCP SYN, parse and add to payload
            if flag_syn:
                fb_query_body.update(p.tcp_signature_parser(version, str(ttl),
                                                            str(header_length - 20), str(wsize),
                                                            options, dnt_frgmnt, id_num == 0, ecn,
                                                            mbz, flow_id == 0, sequence == 0,
                                                            acknowledgement == 0, urg_ptr == 0,
                                                            flag_ack, flag_syn, flag_urg, flag_psh,
                                                            bool(data)))

            # Identify HTTP request, parse and add to payload
            if dst_port == 80:
                fb_query_body.update(p.user_agents_parser(data))
        ######## IP protocol 17 for UDP ########
        if protocol == 17:
            (src_port, dst_port, _size, data) = udp_segment(data)

            # Identify DNS response msg, parse and add to payload
            if dst_port == 53:
                fb_query_body.update(p.destination_hosts_parser(data[2]>>7, data[4:6], data[12:]))

            # Identify DHCP Discover msg, parse and add to payload
            if (dst_port == 67) and (dst == '255.255.255.255'):
                fb_query_body.update(p.dhcp_parser(data))

            # Identify DHCPv6 msg, parse and add to payload
            if dst_port == 547:
                fb_query_body.update(p.dhcp6_parser(data))

            # Identify MDNS msg, parse and add to payload
            if src_port == 5353:
                fb_query_body.update(p.mdns_services_parser(data))

            # Identify SSDP, parse and add to payload
            if src_port == 1900:
                fb_query_body.update(p.upnp_parser(data))

        if len(fb_query_body) > 1:
            fq_macs = list(map(lambda x: x['mac'], fingerprint_querries))
            if fb_query_body['mac'] in fq_macs:
                entry = fingerprint_querries[fq_macs.index(fb_query_body['mac'])]
                combinde_fingerprints(entry, fb_query_body)
            else:
                fingerprint_querries.append(fb_query_body)

        new_time = time.time()
        if new_time > start_time + 10:
            start_time = new_time
            with open('fingerprint_logs', 'a+') as file:
                for fingerprint in fingerprint_querries:
                    file.write(str(fingerprint) + '\n')

def combinde_fingerprints(original, new):
    '''
    Takes two dictionaries containing fingerprints and combinds them.
    The process removes duplicates and limits listable fingerprint features to 5 entries.

    :param orignial: fingerprint dict
    :param new:      fingerprint dict
    :return:         dictionary with cominded fingerprints
    '''
    combindable_fields = ['user_agents', 'destination_hosts', 'mdns_services', 'upnp_user_agents']
    for field in new:
        if field in original and field in combindable_fields:
            original[field] = new[field] + (list(set(original[field]) - set(new[field])))
            original[field] = original[field][:5]
        else:
            original[field] = new[field]
    return original

def ethernet_frame(data):
    '''
    Unpacks a ethernet frame and parses the header data

    :param data: raw bytes containing the ethernet header and data
    :return:     tuple with destination mac, source mac, protocol, and the data portion
    '''
    dst_mac, src_mac, protocol = struct.unpack('! 6s 6s H', data[:14])
    return get_mac_addr(dst_mac), get_mac_addr(src_mac), socket.htons(protocol), data[14:]

def get_mac_addr(bytes_addr):
    '''
    Return properly formatted MAC address (i.e. AA:BB:CC:DD:EE:FF).
    
    :param byte_addr: mac address in raw bytes
    :return:          mac address as a string
    '''
    bytes_str = map('{:02x}'.format, bytes_addr)
    return ':'.join(bytes_str).upper()

def ipv6_packet(data):
    '''
    Unpacks IPv6 packet, parses data from raw bytes to Python types

    :param data: ipv6 packet, header + data portion in raw bytes
    :return:     tuple with ipv6 header info (parsed) and the data protion (not parsed) 
    '''
    version = data[0] >> 4
    flow_id, payload_length, next_header, hop_limit, src, dst = struct.unpack('! I H B B 16s 16s',
                                                                              data[:40])
    return version, flow_id, payload_length, next_header, hop_limit, ipv6(src), ipv6(dst), data[40:]

def ipv6(data):
    '''
    Returns properly formatted IPv6 address
    
    :param data: ipv6 address in raw bytes
    :return:     properly formatted ipv6 address
    '''
    return socket.inet_ntop(socket.AF_INET6, data)

def ipv4_packet(data):
    '''
    Unpacks IPv4 packet: convert bytes to Python types
    
    :param data: ipv4 packet, raw bytes
    :return:     tuple with ipv4 header + rest of data (unparsed)
    '''
    version_header_length = data[0]
    version = version_header_length >> 4
    header_length = (version_header_length & 15) * 4
    ecn = (data[1] & 3)
    flags = data[6]
    must_be_zero = (flags & 128) >> 7
    dont_fragment = (flags & 64) >> 6
    identification, ttl, protocol, src, dst = struct.unpack('! 4x H 2x B B 2x 4s 4s', data[:20])
    return (version, header_length, ecn, identification, must_be_zero, dont_fragment, ttl, protocol,
            ipv4(src), ipv4(dst), data[header_length:])

def ipv4(addr):
    '''
    Returns properly formatted IPv4 address.

    :param addr: ipv4 addres in raw bytes
    :return:     ipv4 address, propperly formatted
    '''
    return socket.inet_ntop(socket.AF_INET, addr)

def tcp_segment(data):
    '''
    Unpacks TCP segment: convert bytes to Python types
    
    :param data: tcp header + data, raw bytes
    :return:     tuple with tcp header info + data (unprocessed)
    '''
    (src_port, dst_port, sequence, acknowledgement, offset_reserved_flags, wsize, checksum,
     urg_ptr) = struct.unpack('! H H L L H H H H', data[:20])
    offset = (offset_reserved_flags >> 12) * 4
    flag_urg = (offset_reserved_flags & 32) >> 5
    flag_ack = (offset_reserved_flags & 16) >> 4
    flag_psh = (offset_reserved_flags & 8) >> 3
    flag_rst = (offset_reserved_flags & 4) >> 2
    flag_syn = (offset_reserved_flags & 2) >> 1
    flag_fin = offset_reserved_flags & 1
    options = data[20:offset]
    return (src_port, dst_port, sequence, acknowledgement, flag_urg, flag_ack, flag_psh, flag_rst,
            flag_syn, flag_fin, wsize, checksum, urg_ptr, options, data[offset:])

def udp_segment(data):
    '''
    Unpacks UDP segment: convert convert bytes to Python types
    
    :param data: udp header + data, raw bytes
    :return:     tuple with udp header info + data (unprocessed)
    '''
    src_port, dst_port, size = struct.unpack('! H H H 2x', data[:8])
    return src_port, dst_port, size, data[8:]

sniff() # Run main method and start the program.