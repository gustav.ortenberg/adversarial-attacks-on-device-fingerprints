'''
This module calculates results from the adversarial attacks.
Tailored to gather the metrics we needed for our thesis.
'''
import ast
import sys


def calculate_perturbation(original, adversarial):
    '''
    Calculates the total peturbation by comparing the original fingerprint
    with the adversarial, character by character.

    :param original:    original sample in string-ified dict format
    :param adversarial: adversarial sample in string-ified dict format
    :return:            perturbation, each character difference adds 1
    '''
    return sum(a != b for a, b in zip(original, adversarial)) + \
        abs(len(original) - len(adversarial))

def calculate_result(result_file_path):
    '''
    Calculates the metrics for adversarial sampling.

    :param result_file_path: path of file containing the results of adversarial sampling
    :return:                 list containing metrics from result calculation
    '''
    total_count_sub = 0
    success_sub = 0
    success_count_sub = 0

    total_count_fb = 0
    success_fb = 0
    success_count_fb = 0

    success_rate_sub = 0.0
    success_rate_fb = 0.0
    transferability = 0.0

    mac_change_count = 0
    mac_change_success = 0

    adv_mac = None
    org_mac = None

    # This stores count for successes
    # index 0: neither sub or fb
    # index 1: sub only
    # index 2: fb only
    # index 3: both
    outcome_matrix = [0, 0, 0, 0]
    try:
        results = open(result_file_path, "r")
    except IOError as error:
        print(
            "Result calculation Error: Could not open file {}: {}".format(
                result_file_path, error))
        sys.exit()

    line = results.readline().replace('\t', '')
    while not line == '':
        if "Org" in line:
            # Fetch original sample
            original = ast.literal_eval(results.readline().replace('\t', ''))
            org_mac = original.pop('mac', None)
            # New sample, reset success param
            success_sub = 0
            success_fb = 0

        elif "Adv" in line:
            # Fetch adversarial sample
            adversarial = ast.literal_eval(results.readline().replace('\t', ''))
            if 'mac' in adversarial.keys():
                adv_mac = adversarial.pop('mac', None)
                if org_mac != adv_mac:
                    # MAC change detected
                    mac_change_count += 1

        elif "Success on Substitute?" in line:
            total_count_sub += 1
            if '1' in line:
                # Success on sub detected
                success_sub = 1
                success_count_sub += 1
        elif "Fingerbank responses" in line:
            total_count_fb += 1
            responses = ast.literal_eval(results.readline().replace('\t', ''))
            # responses[0][1] == adversarial response
            if 'Apple' in str(responses[0][1]):
                # Success on Fb detected
                success_fb = 1
                success_count_fb += 1
                if (None not in [org_mac, adv_mac] and original == adversarial and
                        org_mac != adv_mac):
                    mac_change_success += 1
            outcome_matrix[success_sub+(2*success_fb)] += 1

        line = results.readline().replace('\t', '')
    results.close()

    # Compute sub success rate
    if total_count_sub > 0:
        success_rate_sub = success_count_sub / total_count_sub

    # Compute fb success rate
    if total_count_fb > 0:
        success_rate_fb = success_count_fb / total_count_fb

    # Compute transferability
    if success_count_sub > 0:
        transferability = success_count_fb / success_count_sub
    
    return [total_count_sub, success_rate_sub, total_count_fb, success_rate_fb,
            round(transferability, 3), mac_change_count, mac_change_success, outcome_matrix]

def main():
    '''Prints results to user'''

    folder = input("Enter name of folder containing results, including '/': ")
    theta_min = float(input("Enter theta MIN value (float): "))
    theta_max = float(input("Enter theta MAX value (float): "))
    gamma_min = float(input("Enter gamma MIN value (float): "))
    gamma_max = float(input("Enter gamma MAX value (float): "))

    step_adv = 0.1
    result = []
    theta_adv = theta_min
    gamma_adv = gamma_min

    file_names = [] #Specify the names of your files
    
    # Iterate over all files in the folder
    for file_name in file_names:
        for i in range(1,11):
            file_path = folder + str(i) + '_' + file_name + '_theta_' + theta_adv + '_gamma_' + gamma_adv
            result_list = calculate_result(file_path)
           
            # Calculate results for each file
            result.append("THETA: {}, GAMMA: {}\n".format(theta_adv, gamma_adv))
            result.append("Total # of adv. samples tried on sub: {}\nRate (%) of successful adv. ".
                          format(result_list[0])+
                          "samples on sub: {}\nTotal # of adv. samples tried on Fb: {}\nRate (%) ".
                          format(result_list[1], result_list[2])+
                          "of successful adv. samples on Fb: {}\n".format(result_list[3])+
                          "Transferability: {}\n".format(result_list[4]))
            result.append("----------------------------------\n")
            result.append("Total number of MAC changes: {}\nTotal number of successful MAC changes: {}\n".
                           format(result_list[5], result_list[6]))
            result.append("Outcome matrix: {}\n".format(result_list[7]))
            result.append("----------------------------------\n")
           
            gamma_adv = round(gamma_adv + step_adv, 1)
        gamma_adv = gamma_min
        theta_adv = round(theta_adv + step_adv, 1)

        try:
            res_file = open(folder + "results" + file_name, "w")
        except IOError as error:
            print("Result calculation Error: Could not open file: {}".format(error))
            sys.exit()
        for line in result:
            res_file.write(line)
        res_file.close()
        print("Results written to " + folder + "results")


if __name__ == '__main__':
    main()
