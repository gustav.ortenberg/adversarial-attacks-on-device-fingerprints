'''
This module contains the adversarial sampling generation
'''
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import numpy as np
import tensorflow as tf
from cleverhans.attacks import SaliencyMapMethod
from cleverhans.utils_tf import model_argmax
from cleverhans.utils_keras import KerasModelWrapper
from cleverhans.utils import other_classes

from six.moves import xrange
from keras.models import load_model

import dataprocessing as dp
import fingerbank_api as fb

BATCH_SIZE = 128
NB_CLASSES = 2
SOURCE_SAMPLES = 15
GAMMA = 0.1
THETA = 1

def sampling(gamma=GAMMA, theta=THETA, source_samples=SOURCE_SAMPLES, nb_classes=NB_CLASSES,
             sample_limit=30, itr=1, file_name='fb-results-txt', model_file='model.h5', folder='/'):
    '''
    :param gamma:          float, step size for sample crafting
    :param theta:          float, maximum distortion in a sample
    :param source_samples: int, number of vectors to perturb
    :param nb_classes:     int, number of classes in the Substitute model
    :param sample_limit:   int, number of samples to fetch before terminating data processing
    :param file_name:      string, path to input samples
    :param model_file:     string, path to substitute network model
    :param folder:         string, folder path to input samples
    :return:               Generates source_samples number of adversarial examples and stores 
                           their fingerprint + fingerbank class in a file.
    '''
    sess = tf.Session()
    x_test = []
    y_test = []
    device_class = "Apple"
    dp.process_data(device_class, sample_limit, folder+file_name)
    print("Loading processed data.")
    dataset = np.load("processed.npz", allow_pickle=True)['arr_0']
    for i in range(int(len(dataset))):
        x_test.append(dataset[i][0])
        y_test.append(dataset[i][1])
    x_test = np.array(x_test)
    y_test = np.array(y_test)

    x_tensor = tf.placeholder(tf.float32, shape=(None, 253))

    model_sub = load_model(model_file)
    model_sub = KerasModelWrapper(model_sub)

    preds = model_sub.get_logits(x_tensor)
    # Keep track of success (adversarial example classified in target)
    results = np.zeros((nb_classes, source_samples), dtype='i')

    # Rate of perturbed features for each test set example and target class
    perturbations = np.zeros((nb_classes, source_samples), dtype='f')

    sess.run(tf.global_variables_initializer())

    # Initialize the Fast Gradient Sign Method (FGSM) attack object.
    jsma_params = {'theta': theta, 'gamma': gamma, 'clip_min': 0., 'clip_max': 1.,
                   'y_target': None}
    jsma = SaliencyMapMethod(model_sub, sess=sess)
    adv_array = [] #(adv sample, res against Substitute)
    # Loop over the samples we want to perturb into adversarial examples
    for sample_ind in xrange(source_samples):
        print('--------------------------------------')
        print('Attacking input %i/%i' % (sample_ind + 1, source_samples))
        sample = x_test[sample_ind:(sample_ind + 1)]

        # We want to find an adversarial example for each possible target class
        # (i.e. all classes that differ from the label given in the dataset)
        current_class = int(np.argmax(y_test[sample_ind]))
        target_classes = other_classes(nb_classes, current_class)

        # Loop over all target classes
        for target in target_classes:
            print('Generating adv. example for target class %i' % target)
            # This call runs the Jacobian-based saliency map approach
            one_hot_target = np.zeros((1, nb_classes), dtype=np.float32)
            one_hot_target[0, target] = 1 
            jsma_params['y_target'] = one_hot_target
            adv_x = jsma.generate_np(sample, **jsma_params)

            # Check if success was achieved
            res = int(model_argmax(sess, x_tensor, preds, adv_x) == target)

            # Computer number of modified features
            adv_x_reshape = adv_x.reshape(-1)
            test_in_reshape = x_test[sample_ind].reshape(-1)
            nb_changed = np.where(adv_x_reshape != test_in_reshape)[0].shape[0]
            percent_perturb = float(nb_changed) / adv_x.reshape(-1).shape[0]

            # Update the arrays for later analysis
            results[target, sample_ind] = res
            perturbations[target, sample_ind] = percent_perturb
            adv_array.append((sample, adv_x, res))
    print('--------------------------------------')
    # Compute the number of adversarial examples that were successfully found
    nb_targets_tried = ((nb_classes - 1) * source_samples)
    succ_rate = float(np.sum(results)) / nb_targets_tried
    print('Avg. rate of successful adv. examples {0:.4f}'.format(succ_rate))

    # Compute the average distortion introduced by the algorithm
    percent_perturbed = np.mean(perturbations)
    print('Avg. rate of perturbed features {0:.4f}'.format(percent_perturbed))

    # Compute the average distortion introduced for successful samples only
    percent_perturb_succ = np.mean(perturbations * (results == 1))
    print('Avg. rate of perturbed features for successful '
          'adversarial examples {0:.4f}'.format(percent_perturb_succ))

    with open('{}_{}_theta_{}_gamma_{}'.format(itr, file_name, theta, gamma), 'w+') as file:
        for (og_sample, adv_sample, success) in adv_array:
            adv_fp = dp.vector_to_fingerprint(np.array(list(map(lambda x: 0 if x < 0 else
                                                                (1 if x > 1 else x),
                                                                adv_sample[0]))))
            og_fp = dp.vector_to_fingerprint(og_sample[0])
            adv_class = fb.auto_fb_req([adv_fp], wait_for=12)

            file.write('Org\n\t' + str(og_fp) +
                       '\nAdv\n\t' + str(adv_fp) +
                       '\nSuccess on Substitute? ' + str(success) +
                       '\nFingerbank responses\n\t\t')
            file.write(str(adv_class))
            file.write('\n\n')
    # Close TF session
    sess.close()


SAMPLE_AMOUNT = input("Enter amount of samples for data processing: ")
model_file = 'model.h5'
folder = ''     #specify your folder 
file_list = []  #list your files

for file in file_list:
    for itr in range(1, 11):
        sampling(theta=.1, gamma=.3, file_name=file, folder=folder, itr=itr, sample_limit=SAMPLE_AMOUNT,
                 source_samples=int(SAMPLE_AMOUNT/2), model_file=model_file)
