'''
Module with parsers for each type of fingerprint data
'''
from functools import reduce

def to_int(not_int):
    '''Aux function to perfom conversion into int'''
    return int.from_bytes(not_int, byteorder='big')

def user_agents_parser(blob):
    '''
    Finds all user agents in a http packet

    :param blob: HTTP packet, raw bytes
    :return:     dictionary with user agents
    '''
    blob = blob.decode(errors='ignore')
    query_fields = {}
    header_fields = {}
    for line in blob.split('\r\n'):
        entry = line.split(': ')
        if len(entry) == 2:
            header_fields[entry[0]] = entry[1]
    if "User-Agent" in header_fields:
        query_fields['user_agents'] = [header_fields["User-Agent"]]
    return query_fields

def upnp_parser(blob):
    '''
    Finds all upnp user agents and upnp server strings

    :param blob: SSDP packet, raw bytes
    :return:     dictionary with upnp user agents and upnp server string
    '''
    blob = blob.decode(errors='ignore')
    query_fields = {}
    header_fields = {}
    for line in blob.split('\r\n'):
        entry = line.split(': ')
        if len(entry) == 2:
            header_fields[entry[0]] = entry[1]
    if "USER-AGENT" in header_fields:
        query_fields['upnp_user_agents'] = [header_fields["USER-AGENT"]]
    if "SERVER" in header_fields:
        query_fields['upnp_server_strings'] = header_fields["SERVER"]
    return query_fields

def mdns_services_parser(blob):
    '''
    Finds all mdns services (returns max 5)

    :param blob: MDNS packet, raw bytes
    :return:     dictionary with mdns services
    '''
    query_fields = {}
    #Check QR flag
    if not (blob[2] & 128)>>7:
        return query_fields
    q_count = to_int(blob[4:6])
    ans_count = to_int(blob[6:8])
    data = blob[12:]
    parsed = dns_rr_parser(q_count, ans_count, 0, 0, data)
    if parsed['answers']:
        lst = []
        for (name, _, _, _, _, data) in parsed['answers'][:5]:
            if name:
                lst.append(name)
        query_fields['mdns_services'] = lst 
    return query_fields

def dhcp6_parser(dhcp6_options):
    '''
    Finds the dhcp6 fingerprint and enterprise number

    :param dhcp6_options: dhcp6 header, raw bytes
    :return:              dictionary with dhcp6 fingerprint and enterprise number
    '''
    query_fields = {}
    if not dhcp6_options:
        return query_fields
    fingerprints = ''
    opts = dhcp6_options_parser(dhcp6_options[4:])
    for (op_code, data) in opts:
        if op_code == 6:
            for i in range(0, len(data), 2):
                part = data[i:i+2]
                fingerprints += ',' + str(to_int(part))
            query_fields['dhcp6_fingerprint'] = fingerprints[1:]
        if op_code in [16, 17]:
            query_fields['dhcp6_enterprise'] = str(to_int(data[:4]))
    return query_fields


def dhcp6_options_parser(options):
    '''
    Parse out all dhcp6 options

    :param options: dhcpv6 options, raw bytes
    :return:        list of tuples (op_code, op_data)
    '''
    opts = []
    while len(options) > 3:
        op_code = to_int(options[0:2])
        olen = to_int(options[2:4])
        data = options[4:4+olen]
        opts.append((op_code, data))
        options = options[4+olen:]
    return opts

def dhcp_parser(blob):
    '''
    Finds the dhcp fingerprint and enterprise number

    :param blob: DHCP discover packet, raw bytes
    :return:     dictionary with dhcp fingerprint and vendor number
    '''
    ptr = 0
    query_fields = {}

    # Identify DHCP Option 55 (Parameter Request List)
    while ptr <= len(blob)-1:
        if blob[ptr] == 55:
            param_length = blob[ptr+1]
            data = blob[ptr+2:ptr+2+param_length]
            query_fields['dhcp_fingerprint'] = reduce(lambda x, y: str(x)+','+str(y), data)

        # Identify DHCP Option 60 (Class-identifier)
        if blob[ptr] == 60:
            param_length = blob[ptr+1]
            query_fields['dhcp_vendor'] = blob[ptr+2:ptr+2+param_length].decode(errors='ignore')

        # Identify DHCP Option 12 (Host name)
        if blob[ptr] == 12:
            param_length = blob[ptr+1]
            query_fields['hostname'] = blob[ptr+2:ptr+2+param_length].decode(errors='ignore')

        ptr += 1
    return query_fields

def destination_hosts_parser(op_code, no_queries, data):
    '''
    Finds the destination hosts in data

    :param op_code:    dns option code
    :param no_queries: number of queries
    :param data:       data, raw bytes
    :return:           dictionary with destinations hosts
    '''
    query_fields = {}
    if op_code != 0:
        return query_fields
    no_queries = to_int(no_queries)
    parsed = dns_rr_parser(no_queries, 0, 0, 0, data)
    dest_hosts = []
    for (name, _, _) in parsed['questions']:
        dest_hosts.append(name)
    query_fields['destination_hosts'] = dest_hosts[:5]
    return query_fields

def tcp_signature_parser(ver, ttl, olen, wsize, tcp_opts, dont_fragment,
                         ipid_zero, ecn, must_be_zero, flow_id_zero, seq_zero,
                         ack_zero, urg_zero, ack, syn, urg, psh, pclass):
    '''
    Finds the tcp signatures (syn and syn-ack)

    :param ver:           ip version (4 or 6)
    :param ttl:           ttl
    :param olen:          ip options len
    :param wsize:         window size
    :param tcp_opts:      tcp options data, raw bytes
    :param dont_fragment: true if dont fragment flag == 1 (ipv4 only)
    :param ipid_zero:     true if ip transaction id == 0 false if non-zero (ipv4 only)
    :param ecn:           true if ecn bits are either 10 or 01
    :param must_be_zero:  true if must be zero bit is set (ipv4 only)
    :param flow_id_zero:  true if flow id field is zero (ipv6 only)
    :param seq_zero:      true if sequence number is zero (tcp)
    :param ack_zero:      true if acknowledge number is zero (tcp)
    :param urg_zero:      true if urgent pointer is zero (tcp)
    :param ack:           true if ack flag set
    :param syn:           true if syn flag set
    :param urg:           true if urg flag set
    :param psh:           true if psh flag set
    :param pclass:        true if length of tcp data is non zero
    :return:              dictionary with tcp_syn and tcp_syn_ack signatures
    '''
    query_fields = {}
    tcp_opts = option_parser(tcp_opts)
    if syn == 0:
        return query_fields
    ittl = ttl + '+0'
    mss = '0'
    scale = ''
    olayout = ''
    for (op_code, data) in tcp_opts:
        if str(op_code) == '2' and data:
            mss = str(to_int(data))
        if str(op_code) == '3' and data:
            scale = str(to_int(data))
        olayout += olayout_converter(op_code)
    olayout = olayout[:-1]
    quirks = quirks_converter(ver, dont_fragment, ipid_zero, ecn, must_be_zero,
                              flow_id_zero, seq_zero, ack_zero, urg_zero, ack,
                              urg, psh, tcp_opts)
    pclass = ('+', '0')[pclass]
    #Formatting of p0f signature
    sig = str(ver) + ':' + ittl + ':' + olen + ':' + mss + ':' + wsize + ',' + scale
    sig += ':' + olayout + ':' + quirks + ':' + pclass
    query_fields = ({'tcp_syn_signatures':sig}, {'tcp_syn_ack_signatures':sig})[syn and ack]
    return query_fields

def option_parser(blob):
    '''
    Finds all tcp options in the data

    :param blob: TCP option data field
    :return:     list of (op-code, op-data) tuples
    '''
    options = []
    while blob:
        index = 0
        bad = False
        op_code = str(blob[0])
        if op_code in ['0', '1']:
            index += 1
            options.append((op_code, (len(blob)-1, 'null')[op_code == '1']))
        elif len(blob) < 2:
            bad = True
        else:
            msg_len = blob[1]
            if (op_code not in ['2', '3', '4', '5', '8'] or msg_len < 2 or
                    msg_len > 40 or len(blob) < msg_len):
                bad = True
            if op_code == '5' and (msg_len < 10 or msg_len > 34):
                bad = True
            index = (blob[1], len(blob))[bad]
            options.append((op_code, ('', blob[2:index])[index > 2]))
            if bad:
                options.append(('bad', 'bad'))
        blob = blob[index:]
    return options

def olayout_converter(op_code):
    '''
    Maps tcp opcodes to olayout keywords

    :param op_code: tcp option number
    :return:        olayout keyword
    '''
    keyword = '?' + str(op_code) + ',' #unknown option ID n
    if op_code == '0':
        keyword = 'eol+,'
    if op_code == '1':
        keyword = 'nop,'
    if op_code == '2':
        keyword = 'mss,'
    if op_code == '3':
        keyword = 'ws,'
    if op_code == '4':
        keyword = 'sok,'
    if op_code == '5':
        keyword = 'sack,' 
    if op_code == '8':
        keyword = 'ts,'
    if op_code == 'bad':
        keyword = ''
    return keyword

def quirks_converter(ver, dont_fragment, ipid_zero, ecn, must_be_zero,
                     flow_id_zero, seq_zero, ack_zero, urg_zero, ack, urg, psh,
                     tcp_opts):
    '''
    Produces a quirks string

    :param ver:           ip version
    :param dont_fragment: true if dont fragment flag == 1 (ipv4 only)
    :param ipid_zero:     true if ip transaction id == 0  (ipv4 only)
    :param ecn:           true if ecn bits are either 10 or 01
    :param must_be_zero:  true if must be zero bit is set (ipv4 only)
    :param flow_id_zero:  true if flow id field is zero (ipv6 only)
    :param seq_zero:      true if sequence number is zero (tcp)
    :param ack_zero:      true if acknowledge number is zero (tcp)
    :param urg_zero:      true if urgent pointer is zero (tcp)
    :param ack:           true if ack flag set
    :param urg:           true if urg flag set
    :param psh:           true if psh flag set
    :param tcp_opts:      dict {option:len+data}
    :return:              quirk string
    '''
    keywords = ''
    if ver == 4:
        keywords += ('', 'dont_fragment,')[dont_fragment]
        keywords += ('', 'id+,')[dont_fragment and not ipid_zero]
        keywords += ('', 'id-,')[not dont_fragment and ipid_zero]
        keywords += ('', 'ecn,')[ecn not in [3, 0]]
        keywords += ('', '0+,')[must_be_zero]
    if ver == 6:
        keywords += ('', 'ecn,')[ecn not in [3, 0]]
        keywords += ('', 'flow,')[not flow_id_zero]
    #TCP related quirks
    keywords += ('', 'seq-,')[seq_zero]
    keywords += ('', 'ack+,')[not ack_zero and not ack]
    keywords += ('', 'ack-,')[ack_zero and ack]
    keywords += ('', 'uptr+,')[not urg_zero and not urg]
    keywords += ('', 'urgf+,')[urg]
    keywords += ('', 'pushf+,')[psh]
    if '8' in tcp_opts:
        keywords += ('', 'ts1-,')[tcp_opts['8'][:4] == b'\x00'*4]
        keywords += ('', 'ts2+,')[not tcp_opts['8'][4:] == b'\x00'*4]
    if '0' in tcp_opts:
        keywords += ('', 'opt+,')[not tcp_opts['0'] == 0]
    if '3' in tcp_opts and tcp_opts['3'] > 14:
        keywords += 'exws,'
    # BAD checks
    bad = '2' in tcp_opts and not len(tcp_opts['2']) == 2
    bad = bad or '3' in tcp_opts and not len(tcp_opts['3']) == 1
    bad = bad or '4' in tcp_opts and not len(tcp_opts['4']) == 0
    bad = bad or '8' in tcp_opts and not len(tcp_opts['8']) == 8
    bad = bad or 'bad' in tcp_opts
    keywords += ('', 'bad,')[bad] # malformed TCP options present
    if keywords:
        keywords = keywords[:-1]
    return keywords

def dns_rr_parser(q_count, ans_count, auth_count, add_count, data):
    '''
    Parses DNS Resource Record requests/answers

    :param q_count:    number of questions in :param data:
    :param ans_count:  number of answer in :param data:
    :param auth count: number of authority RRs in :param data:
    :param add_count:  number of additional RRs in :param data:
    :param data:       raw bytes with all RRs
    :return:           ditctionary with RRs separated by type
    '''
    parsed = {'questions':[], 'answers':[], 'authority':[], 'additional':[]}
    while q_count != 0:
        qname = ''
        keep_running = True
        while keep_running and data and data[0] != 0:
            if data[0] == 192:
                keep_running = False
                data = data[2:]
            else:
                length = data[0]
                qname += data[1:1+length].decode(errors='ignore') + '.'
                data = data[1+length:]
        qname = qname[:-1]
        parsed['questions'].append((qname, data[1:3], data[3:5]))
        data = data[5:]
        q_count -= 1
    while data:
        rr_name = ''
        while data and data[0] != 0:
            if data[0] == 192:
                keep_running = False
                data = data[2:]
            else:
                length = data[0]
                rr_name += data[1:1+length].decode(errors='ignore')+'.'
                data = data[1+length:]
        rr_name = rr_name[:-1]
        data = data[1:]
        rr_type = data[:2]
        rr_class = data[2:4]
        rr_ttl = data[4:8]
        rr_rdlen = to_int(data[8:10])
        data = data[10:]
        rr_rdata = data[:rr_rdlen]
        data = data[rr_rdlen:]
        if ans_count != 0:
            ans_count -= 1
            parsed['answers'].append((rr_name, rr_type, rr_class, rr_ttl, rr_rdlen, rr_rdata))
        elif auth_count != 0:
            auth_count -= 1
            parsed['authority'].append((rr_name, rr_type, rr_class, rr_ttl, rr_rdlen, rr_rdata))
        elif add_count != 0:
            add_count -= 1
            parsed['additional'].append((rr_name, rr_type, rr_class, rr_ttl, rr_rdlen, rr_rdata))
    return parsed
