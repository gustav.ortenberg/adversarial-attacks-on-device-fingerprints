'''
This module preprocesses fingerprint data from collected packets to fit the TensorFlow model.
'''
import ast
import sys
from decimal import Decimal, ROUND_HALF_UP, getcontext
import numpy as np
import fb_names as n

MDNS_DICTIONARY = {}
MDNS_DICTIONARY_REV = {}
DEST_HOSTS_DICTIONARY = {}
DEST_HOSTS_DICTIONARY_REV = {}
HTTP_USER_AGENT_DICTIONARY = {}
HTTP_USER_AGENT_DICTIONARY_REV = {}
UPNP_USER_AGENT_DICTIONARY = {}
UPNP_USER_AGENT_DICTIONARY_REV = {}
UPNP_SERVER_STRING_DICTIONARY = {}
UPNP_SERVER_STRING_DICTIONARY_REV = {}
DHCP_VENDOR_DICTIONARY = {}
DHCP_VENDOR_DICTIONARY_REV = {}
HOSTNAME_DICTIONARY = {}
HOSTNAME_DICTIONARY_REV = {}
QUIRK_LIST = ['df', 'id+', 'id-', 'ecn', '0+', 'flow', 'seq-', 'ack+', 'ack-', 'uptr+', 'urgf+',
              'pushf+', 'ts1-', 'ts2+', 'opt+', 'exws', 'bad']
OLAYOUT_LIST = ['eol+n', 'nop', 'mss', 'ws', 'sok', 'sack', 'ts', '?n']

def process_data(class_name, sample_limit=30, file_name='training_data.txt'):
    '''
    Transforms a number of fingerprints into feature vectors. If their device class
    matches the specified one they are labeled as a target otherwise not a target.

    :param class_name:   target device class
    :param sample_limit: number of samples to fetch before terminating loop
    :param file_name:    file to read samples from
    '''
    labels = []
    fingerprints = []
    try:
        file = open(file_name, 'r')
    except IOError as error:
        print('Data fetching error: Unable to open file: {}'.format(error))
        sys.exit()

    counter = 0
    print('Loading data.')
    for line in file:
        if counter == sample_limit:
            break
        # Split into response and fingerprint, Re-attach missing bracket, and interpret as dicts
        linelist = line.split('}{')
        response = ast.literal_eval(linelist[0] + '}')
        fingerprint = ast.literal_eval('{' + linelist[1])
        try:
            label = response['device_name']
        except KeyError:
            continue
        labels.append(label)
        fingerprints.append(fingerprint)
        counter += 1

    file.close()
    if len(labels) != len(fingerprints):
        print('Data fetching error: non-matching label-fingerprint lenghts. ',
              'Labels: {}, fingerprints: {}. Exiting.'.format(len(labels), len(fingerprints)))
        sys.exit()
    print('Data loaded.')
    print('Initiating data processing.')
    print('----> Constructing dictionaries.')
    build_all_dicts()
    print('----> ...complete.')
    print('----> Processing data...')
    data_tuples = []
    for counter, value in enumerate(labels):
        data_tuples.append((fingerprint_to_vector(fingerprints[counter]),
                            label_to_vector(value, class_name)))
    np.savez('processed', data_tuples, allow_pickle=True)

    print('----> ...complete.')
    print('Data processing complete.')
    print('Processed data located in file processed.npz')


def label_to_vector(label, class_name):
    '''
    :param label:      label of a fingerprint
    :param class_name: target class
    :return:           [0,1] the label represents the target class, [1,0] otherwise
    '''
    return np.array([0, 1]) if class_name in label else np.array([1, 0])

def build_all_dicts():
    '''Builds all globally defined dictionaries from the file fb-results-txt'''
    try:
        file = open('fb-results.txt', 'r')
    except IOError as error:
        print('Preprocessing Error: Unable to open file: {}'.format(error))
        sys.exit()
    for line in file:
        linelist = line.split('}{')
        linelist[0] = ''
        fingerprint = '{' + linelist[1]
        fingerprint = ast.literal_eval(fingerprint)

        for key in fingerprint.keys():
            if key == n.mdns_serv:
                build_mdns_dict(fingerprint[key])
            elif key == n.dest_host:
                build_dest_hosts_dict(fingerprint[key])
            elif key == n.ua:
                build_http_ua_dict(fingerprint[key])
            elif key == n.upnp_ua:
                build_upnp_user_agents_dict(fingerprint[key])
            elif key == n.upnp_serv:
                build_upnp_server_string_dict(fingerprint[key])
            elif key == n.dhcp_ven:
                build_dhcp_vendor_dict(fingerprint[key])
            elif key == n.hostname:
                build_hostname_dict(fingerprint[key])
    file.close()
    try:
        file = open('dictionaries.txt', 'w')
    except IOError as error:
        print('Preprocessing Error: unable to open file: {}'.format(error))
        sys.exit()
    file.write('####################### MDNS DICTIONARY #######################\n')
    file.write(str(MDNS_DICTIONARY)+'\n')
    file.write('####################### DESTINATION HOSTS DICTIONARY ##########\n')
    file.write(str(DEST_HOSTS_DICTIONARY)+'\n')
    file.write('####################### HTTP USER AGENT DICTIONARY ############\n')
    file.write(str(HTTP_USER_AGENT_DICTIONARY)+'\n')
    file.write('####################### UPNP USER AGENT DICTIONARY ############\n')
    file.write(str(UPNP_USER_AGENT_DICTIONARY)+'\n')
    file.write('####################### UPNP SERVER STRING DICT ###############\n')
    file.write(str(UPNP_SERVER_STRING_DICTIONARY)+'\n')
    file.write('####################### DHCP VENDOR DICTIONARY ################\n')
    file.write(str(DHCP_VENDOR_DICTIONARY)+'\n')
    file.write('####################### HOSTNAME DICTIONARY ###################\n')
    file.write(str(HOSTNAME_DICTIONARY)+'\n')

def fingerprint_to_vector(fingerprint):
    '''
    :param fingerprint: dictionary with a fingerprint
    :return:            feature vector representation of fingerprint
   '''
    http_ua = convert_http_ua(fingerprint.get(n.ua, ''))
    dhcp_fp = convert_dhcp_fingerprint(fingerprint.get(n.dhcp_fp, ''))
    dhcp6_fp = convert_dhcp6_fingerprint(fingerprint.get(n.dhcp6_fp, ''))
    dhcp_ven = convert_dhcp_vendor(fingerprint.get(n.dhcp_ven, ''))
    dhcp6_ent = convert_dhcp6_enterprise(fingerprint.get(n.dhcp6_ent, ''))
    mac = convert_mac(fingerprint.get(n.mac, ''))
    dests = convert_destination_hosts(fingerprint.get(n.dest_host, ''))
    mdns_services = convert_mdns(fingerprint.get(n.mdns_serv, ''))
    upnp_ua = convert_upnp_user_agent(fingerprint.get(n.upnp_ua, ''))
    upnp_srv = convert_upnp_server(fingerprint.get(n.upnp_serv, ''))
    syn_sig = convert_tcp_sign(fingerprint.get(n.syn_sig, ''))
    syn_ack_sig = convert_tcp_sign(fingerprint.get(n.syn_ack_sig, ''))
    hostn = convert_hostname(fingerprint.get(n.hostname, ''))
    lists = http_ua + dhcp_fp + dhcp6_fp + [dhcp_ven] + [dhcp6_ent] + [mac] + dests
    lists += mdns_services + upnp_ua + [upnp_srv] + syn_sig + syn_ack_sig + [hostn]
    return np.array(lists)

def vector_to_fingerprint(vector):
    '''
    :param vector: feature vector representation of fingerprint
    :return:       dictionary representation of fingerprint
    '''
    vector = vector.tolist()
    http_ua = invert_http_ua(vector[:5])
    dhcp_fp = invert_dhcp_fingerprint(vector[5:125])
    dhcp6_fp = invert_dhcp6_fingerprint(vector[125:145])
    dhcp_ven = invert_dhcp_vendor(vector[145])
    dhcp6_ent = invert_dhcp6_enterprise(vector[146])
    mac = invert_mac(vector[147])
    dests = invert_destination_hosts(vector[148:153])
    mdns_services = invert_mdns(vector[153:158])
    upnp_ua = invert_upnp_user_agent(vector[158:163])
    upnp_srv = invert_upnp_server(vector[163])
    syn_sig = invert_tcp_sign(vector[164:208])
    syn_ack_sig = invert_tcp_sign(vector[208:252])
    hostn = invert_hostname(vector[252])
    fingerprint = {}
    if http_ua != []:
        fingerprint[n.ua] = http_ua
    if dhcp_fp != '':
        fingerprint[n.dhcp_fp] = dhcp_fp
    if dhcp6_fp != '':
        fingerprint[n.dhcp6_fp] = dhcp6_fp
    if dhcp_ven is not None:
        fingerprint[n.dhcp_ven] = dhcp_ven
    if dhcp6_ent is not None:
        fingerprint[n.dhcp6_ent] = dhcp6_ent
    if mac != '':
        fingerprint[n.mac] = mac
    if dests != []:
        fingerprint[n.dest_host] = dests
    if mdns_services != []:
        fingerprint[n.mdns_serv] = mdns_services
    if upnp_ua != []:
        fingerprint[n.upnp_ua] = upnp_ua
    if upnp_srv is not None:
        fingerprint[n.upnp_serv] = upnp_srv
    if syn_sig is not None:
        fingerprint[n.syn_sig] = syn_sig
    if syn_ack_sig is not None:
        fingerprint[n.syn_ack_sig] = syn_ack_sig
    if hostn is not None:
        fingerprint[n.hostname] = hostn
    return fingerprint

###################### DEST. HOSTS ###############
def build_dest_hosts_dict(destination_hosts):
    '''
    Fills the global DEST_HOSTS_DICTIONARY

    :param destination_hosts: list of destination hosts
    '''
    global DEST_HOSTS_DICTIONARY, DEST_HOSTS_DICTIONARY_REV
    for host in destination_hosts:
        if host not in DEST_HOSTS_DICTIONARY.keys():
            DEST_HOSTS_DICTIONARY[host] = len(DEST_HOSTS_DICTIONARY)+1
    DEST_HOSTS_DICTIONARY_REV = {v: k for k, v in DEST_HOSTS_DICTIONARY.items()}

def convert_destination_hosts(dest_hosts):
    '''
    :param dest_hosts: list of destination host strings
    :return:           feature vector representation of destination hosts
    '''
    dh_vector = [0 for i in range(5)]
    dh_list = list(map(lambda x: DEST_HOSTS_DICTIONARY.get(x, 0)
                       /len(DEST_HOSTS_DICTIONARY), dest_hosts))
    return dh_list + dh_vector[len(dh_list):]

def invert_destination_hosts(dh_vector):
    '''
    :param dh_vector: feature vector representation of destination hosts
    :return:          list of destination host strings
    '''
    getcontext().rounding = ROUND_HALF_UP
    indecies = list(map(lambda x: round(Decimal(x*len(DEST_HOSTS_DICTIONARY))), dh_vector))
    if 0 in indecies:
        indecies = indecies[:indecies.index(0)]
    dh_list = list(map(lambda x: DEST_HOSTS_DICTIONARY_REV[x], indecies))
    return dh_list

###################### UPNP ######################
def build_upnp_user_agents_dict(user_agents):
    '''
    Fills the global UPNP_USER_AGENT_DICTIONARY
    :param user_agents: list of upnp user agents
    '''
    global UPNP_USER_AGENT_DICTIONARY, UPNP_USER_AGENT_DICTIONARY_REV
    for agent in user_agents:
        if agent not in UPNP_USER_AGENT_DICTIONARY.keys():
            UPNP_USER_AGENT_DICTIONARY[agent] = len(UPNP_USER_AGENT_DICTIONARY)+1
    UPNP_USER_AGENT_DICTIONARY_REV = {v: k for k, v in UPNP_USER_AGENT_DICTIONARY.items()}

def convert_upnp_user_agent(user_agents):
    '''
    :param user_agents: list of upnp user agents
    :return:            feature vector representation of upnp user agents
    '''
    ua_vector = [0.0 for i in range(5)]
    ua_list = list(map(lambda x: UPNP_USER_AGENT_DICTIONARY.get(x, 0)
                       /len(UPNP_USER_AGENT_DICTIONARY), user_agents))
    return ua_list + ua_vector

def invert_upnp_user_agent(ua_vector):
    '''
    :param ua_vector: feature vector representation of upnp user agents
    :return:          list of upnp user agents
    '''
    getcontext().rounding = ROUND_HALF_UP
    indecies = list(map(lambda x: round(Decimal(x*len(UPNP_USER_AGENT_DICTIONARY))), ua_vector))
    if 0 in indecies:
        indecies = indecies[:indecies.index(0)]
    ua_list = list(map(lambda x: UPNP_USER_AGENT_DICTIONARY_REV[x], indecies))
    return ua_list

def build_upnp_server_string_dict(server_string):
    '''
    Fills the global UPNP_SERVER_STRING_DICTIONARY
    :param server_string: upnp server string
    '''
    global UPNP_SERVER_STRING_DICTIONARY, UPNP_SERVER_STRING_DICTIONARY_REV
    if server_string not in UPNP_SERVER_STRING_DICTIONARY.keys():
        UPNP_SERVER_STRING_DICTIONARY[server_string] = len(UPNP_SERVER_STRING_DICTIONARY)+1
        UPNP_SERVER_STRING_DICTIONARY_REV = {v: k for k, v in UPNP_SERVER_STRING_DICTIONARY.items()}

def convert_upnp_server(server):
    '''
    :param server: server string
    :return:       feature vector representation of server string
    '''
    s_elem = 0
    if server != '':
        s_elem = (UPNP_SERVER_STRING_DICTIONARY.get(server, 0))/len(UPNP_SERVER_STRING_DICTIONARY)
    return s_elem

def invert_upnp_server(s_elem):
    '''
    :param s_elem: feature vector representation of server string
    :return:       server string
    '''
    getcontext().rounding = ROUND_HALF_UP
    index = round(s_elem*len(UPNP_SERVER_STRING_DICTIONARY))
    if index == 0:
        return None
    server = UPNP_SERVER_STRING_DICTIONARY_REV[index]
    return server

###################### HTTP UA ###################
def build_http_ua_dict(http_useragents):
    '''
    Fills the global HTTP_USER_AGENT_DICTIONARY
    :param http_useragents: list of http user agents
    '''
    global HTTP_USER_AGENT_DICTIONARY, HTTP_USER_AGENT_DICTIONARY_REV
    for agent in http_useragents:
        if agent not in HTTP_USER_AGENT_DICTIONARY:
            HTTP_USER_AGENT_DICTIONARY[agent] = len(HTTP_USER_AGENT_DICTIONARY)+1
            HTTP_USER_AGENT_DICTIONARY_REV = {v: k for k, v in HTTP_USER_AGENT_DICTIONARY.items()}

def convert_http_ua(http_useragents):
    '''
    :param http_useragents: list of http user agents
    :return:                feature vector representation of http user agents
    '''
    converted_http_uas = [0.0 for i in range(5)]
    if http_useragents != '':
        i = 0
        for agent in http_useragents:
            converted_http_uas[i] = (HTTP_USER_AGENT_DICTIONARY.get(agent, 0)
                                     /len(HTTP_USER_AGENT_DICTIONARY))
            i += 1
            if i == 5:
                break
    return converted_http_uas

def invert_http_ua(http_useragent_floats):
    '''
    :param http_useragent_floats: feature vector representation of http user agents
    :return:                      list of http user agents
    '''
    getcontext().rounding = ROUND_HALF_UP
    inverted_http_uas = []
    for value in http_useragent_floats:
        index = round(Decimal(value*len(HTTP_USER_AGENT_DICTIONARY_REV)))
        if index != 0:
            inverted_http_uas.append(HTTP_USER_AGENT_DICTIONARY_REV[index])
    return inverted_http_uas

###################### DCHP6 ENTERP. #############
def convert_dhcp6_enterprise(enterprise):
    '''
    :param enterprise: dhcp6 enterprise number
    :return:           feature vector representation of the eneterprise number
    '''
    e_element = 0
    if enterprise != '':
        e_element = (int(enterprise)+1)/4294967296
    return e_element

def invert_dhcp6_enterprise(e_element):
    '''
    :param e_element: feature vector representation of the enterprise number
    :return:          dhcp6 eneterprise number
    '''
    getcontext().rounding = ROUND_HALF_UP
    enterprise = None
    if e_element != 0:
        enterprise = str(round(Decimal(e_element*4294967296)-1, 0))
    return enterprise

###################### DCHP6 FP ##################
def convert_dhcp6_fingerprint(fingerprint):
    '''
    :param fingerprint: string representation of dhcpv6 fingerprint
    :return:            feature vector representation of dhcpv6 fingerprint
    '''
    fp_vector = [0.0 for i in range(20)]
    fp_list = []
    if fingerprint != '':
        fp_list = fingerprint.split(',')
        fp_list = list(map(lambda x: (int(x)+1)/65536, fp_list))
    fp_vector[:len(fp_list)] = fp_list
    return fp_vector

def invert_dhcp6_fingerprint(fp_vector):
    '''
    :param fp_vector: feature vector representation of dhcpv6 fingerprint
    :return:          string representation of dhcpv6 fingerprint
    '''
    getcontext().rounding = ROUND_HALF_UP
    if 0.0 in fp_vector:
        fp_vector = fp_vector[:fp_vector.index(0.0)]
    fp_list = list(map(lambda x: str(round(Decimal(x*65536)-1, 0)), fp_vector))
    fingerprint = ','.join(fp_list)
    return fingerprint

###################### DCHP VENDOR ###############
def build_dhcp_vendor_dict(vendor):
    '''
    Fills the global DHCP_VENDOR_DICTIONARY
    :param vendor: dhcp vendor name
    '''
    global DHCP_VENDOR_DICTIONARY, DHCP_VENDOR_DICTIONARY_REV
    if vendor not in DHCP_VENDOR_DICTIONARY.keys():
        DHCP_VENDOR_DICTIONARY[vendor] = len(DHCP_VENDOR_DICTIONARY)+1
        DHCP_VENDOR_DICTIONARY_REV = {v: k for k, v in DHCP_VENDOR_DICTIONARY.items()}

def convert_dhcp_vendor(vendor):
    '''
    :param vendor: string with vendor name
    :return:       feature vector representation of vendor name
    '''
    v_element = 0
    if vendor != '':
        v_element = (DHCP_VENDOR_DICTIONARY.get(vendor, 0))/len(DHCP_VENDOR_DICTIONARY)
    return v_element

def invert_dhcp_vendor(v_element):
    '''
    :param v_element: feature vector representation of vendor name
    :return:          string with vendor name
    '''
    getcontext().rounding = ROUND_HALF_UP
    vendor = None
    index = round(Decimal(v_element*len(DHCP_VENDOR_DICTIONARY)))
    if index != 0:
        vendor = DHCP_VENDOR_DICTIONARY_REV[index]
    return vendor

###################### DCHP FP ###################
def convert_dhcp_fingerprint(fingerprint):
    '''
    :param fingerprint: string representation of dhcp fingerprint
    :return:            feature vector representation of dhcp fingerprint
    '''
    fp_vector = [0 for i in range(120)]
    fp_list = []
    if fingerprint != '':
        fp_list = fingerprint.split(',')
        fp_list = list(map(lambda x: (int(x) + 1)/256, fp_list))
    fp_vector[0:len(fp_list)] = fp_list
    return fp_vector

def invert_dhcp_fingerprint(fp_vector):
    '''
    :param fp_vector: feature vector representation of dhcp fingerprint
    :return:          string representation of dhcp fingerprint
    '''    
    getcontext().rounding = ROUND_HALF_UP
    if 0.0 in fp_vector:
        fp_vector = fp_vector[:fp_vector.index(0.0)]
    fp_list = list(map(lambda x: str(round(Decimal(x*256)-1, 0)), fp_vector))
    fingerprint = ','.join(fp_list)
    return fingerprint

###################### MAC #######################
def convert_mac(mac_address):
    '''
    :param mac_address: mac address string
    :return:            feature vector representation of mac address
    '''
    mac_int = 0
    if mac_address != '':
        mac_address = mac_address.replace(':', '')
        mac_int = int(mac_address, 16)
    return mac_int/281474976710655

def invert_mac(mac):
    '''
    :param mac: feature vector representation of mac address
    :return:    mac address string
    '''
    mac_address = ''
    mac = hex(int(mac*281474976710655))[2:]
    for counter, value in enumerate(mac):
        mac_address += value
        if (counter % 2) == 1:
            mac_address += ':'
    return mac_address[:-1]

###################### TCP #######################
def convert_tcp_sign(tcp_signature):
    '''
    :param tcp_signature: tcp signature as string
    :return:              tcp signature as feature vector
    '''
    converted_tcp_list = [0 for i in range(44)]
    if tcp_signature != '':
        parts = tcp_signature.split(':')
        #Version number
        converted_tcp_list[0] = 0 if parts[0] == '4' else 1
        #ittl
        converted_tcp_list[1] = int(parts[1].split('+')[0])/255
        #tcp options len
        converted_tcp_list[2] = int(parts[2])/10
        #mss value
        converted_tcp_list[3] = int(parts[3])/65535
        #window size and window scale
        wsize, scale = parts[4].split(',')
        if scale == '':
            scale = -1
        converted_tcp_list[4] = int(wsize)/65535
        converted_tcp_list[5] = (int(scale)+1)/256
        #tcp options layout
        olayout = parts[5].split(',')
        for counter, value in enumerate(olayout):
            converted_tcp_list[6+counter] = (OLAYOUT_LIST.index(value)+1)/len(OLAYOUT_LIST)
        #IP and TCP quirks
        q_list = parts[6].split(',')
        for j in range(17):
            if QUIRK_LIST[j] in q_list:
                converted_tcp_list[26+j] = 1
        #Packet class
        if len(parts) >= 8:
            converted_tcp_list[43] = 0 if parts[7] == '0' else 1
    return converted_tcp_list

def invert_tcp_sign(vec):
    '''
    :param vec: tcp signature as feature
    :return:    tcp signature as string
    '''
    getcontext().rounding = ROUND_HALF_UP
    tcp_signature = None
    if vec != [0 for i in range(44)]:
        tcp_signature = ''
        #Version number
        tcp_signature += '4:' if round(Decimal(vec[0])) == 0 else '6:'
        #ittl
        tcp_signature += str(int(round(Decimal(vec[1]*255)))) + '+0:'
        #tcp options len
        tcp_signature += str(int(round(Decimal(vec[2]*10)))) + ':'
        #mss value
        tcp_signature += str(int(round(Decimal(vec[3]*65535)))) + ':'
        #window size and window scale
        wsize = str(int(round(Decimal(vec[4]*65535))))
        scale = int(round(Decimal(vec[5]*256)))-1
        scale = '' if scale == -1 else str(scale)
        tcp_signature += wsize + ', ' + scale + ':'
        #tcp options layout
        for i in range(20):
            option = int(round(Decimal(vec[6+i]*len(OLAYOUT_LIST)))-1)
            if option >= 0:
                tcp_signature += OLAYOUT_LIST[option] + ','
        tcp_signature = tcp_signature[:-1] + ':'
        #IP and TCP quirks
        for i in range(17):
            if round(Decimal(vec[26+i])) == 1:
                tcp_signature += QUIRK_LIST[i] + ','
        tcp_signature = tcp_signature[:-1] + ':'
        #Packet class
        tcp_signature += '0' if round(Decimal(vec[43])) == 0 else '+'
    return tcp_signature

###################### MDNS ######################
def build_mdns_dict(mdns_services):
    '''
    Fills the global MDNS_DICTIONARY
    :param mdns_services: list of mdns services
    '''
    global MDNS_DICTIONARY, MDNS_DICTIONARY_REV
    for service in mdns_services:
        if service not in MDNS_DICTIONARY.keys():
            MDNS_DICTIONARY[service] = len(MDNS_DICTIONARY)+1
    MDNS_DICTIONARY_REV = {v: k for k, v in MDNS_DICTIONARY.items()}

def convert_mdns(mdns_services):
    '''
    :param mdns_services: list of mdns services
    :return:              feature vector representation of mdns services
    '''
    converted_mdns_list = [0.0 for i in range(5)]
    if mdns_services != '':
        i = 0
        for service in mdns_services:
            if service not in MDNS_DICTIONARY.keys():
                print('MDNS conversion error: {} not in dictionary keys. Skip.'.format(service))
                continue
            converted_mdns_list[i] = MDNS_DICTIONARY[service]/(len(MDNS_DICTIONARY))
            i += 1
            if i == 5:
                break
    return converted_mdns_list

def invert_mdns(mdns_floats):
    '''
    :param mdns_floats: feature vector representation of mdns services
    :return:            list of mdns services
    '''
    getcontext().rounding = ROUND_HALF_UP
    inverted_mdns = []
    for value in mdns_floats:
        int_val = round(Decimal(value * len(MDNS_DICTIONARY_REV)))
        if int_val != 0:
            inverted_mdns.append(MDNS_DICTIONARY_REV[int_val])
    return inverted_mdns

###################### Hostname ##################
def build_hostname_dict(hostname):
    '''
    Fills the global HOSTNAME_DICTIONARY
    :param hostname: hostname
    '''
    global HOSTNAME_DICTIONARY, HOSTNAME_DICTIONARY_REV
    if hostname not in HOSTNAME_DICTIONARY.keys():
        HOSTNAME_DICTIONARY[hostname] = len(HOSTNAME_DICTIONARY.keys())+1
        HOSTNAME_DICTIONARY_REV = {v: k for k, v in HOSTNAME_DICTIONARY.items()}

def convert_hostname(hostname):
    '''
    :param hostname: string representation
    :return:         feature vector representation
    '''
    converted_hostname = 0
    if hostname != '':
        if hostname not in HOSTNAME_DICTIONARY.keys():
            print('Hostname conversion error: {} not in dictionary keys. Skip.'.format(hostname))
        else:
            converted_hostname = HOSTNAME_DICTIONARY[hostname]/len(HOSTNAME_DICTIONARY)
    return converted_hostname

def invert_hostname(host_float):
    '''
    :param host_floar: feature vector representation
    :return:           string representation
    '''
    getcontext().rounding = ROUND_HALF_UP
    if host_float == 0:
        return None
    int_key = round(Decimal(host_float * len(HOSTNAME_DICTIONARY_REV)))
    return HOSTNAME_DICTIONARY_REV[int_key] if int_key != 0 else None
