'''
This module provides functions for sending api requests to fingerbank API
'''
import time
import requests

API = "https://api.fingerbank.org/api/v2/combinations/interrogate"
TIME_SINCE_LAST_REQ = time.time()

def fb_request(api_key, payload):
    '''
    Creates HTTP POST request to fingerbank API and returns the response if
    successful otherwise an error message.
    
    :param api_key: API key as a string
    :param payload: dictionary with fingerprint data
    :return:        tuple with (successful?, response)
    '''
    try:
        res = requests.post(API, params=api_key, json=payload)
    except requests.exceptions.RequestException as error:
        return False, error
    if res.status_code == 200:
        return True, res.json()
    error_msg = "Request failed with status code: {} and error {}".format(res.status_code,
                                                                          res.content)
    return False, error_msg

def interrogate(payload, api_key):
    '''
    Creates request to fingerbanks Interrogate API and returns the response if
    successful otherwise an error message.
    
    :param api_key: API key as a string
    :param payload: dictionary with fingerprint data
    :return:        tuple with (successful?, response)
    '''
    error_msg = ""
    success, result = fb_request(api_key, payload)
    if not success:
        error_msg = "Interrogate Error: failed to contact fb: {}".format(result)
        return False, error_msg
    return True, result

def auto_fb_req(fingerprints, wait_for=13):
    '''
    Takes a list of fingerprints and labels them.
    If no label is given by FB the default value is 'Not a class'.
    
    :param fingerprints: list of fingerprints to be labeled
    :param wait_for:     seconds to wait inbetween each request
    :return:             [(fingerprint, label)] 
    '''
    labeled = []
    fingerprints.reverse()
    for fingerprint in fingerprints:
        found = False
        while not found:
            success, res_msg = interrogate(fingerprint,
                                           "key=87fddc806fa5dee92d78cf39c63f8dbb84d73def")
            if success:
                labeled.append((fingerprint, res_msg))
                found = True
            else:
                if 'Try adding more parameters to your query' in res_msg:
                    labeled.append((fingerprint, 'Not a class'))
                    found = True
            time.sleep(wait_for)
    return labeled
